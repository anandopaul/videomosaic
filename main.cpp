#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


class OpenCVLibrary {
private:
	bool _initialized = false;
	VideoCapture _videoCapture;
	Mat _current_frame;
	int _frame_height = 0;
	int _frame_width = 0;

public:
	OpenCVLibrary(char* path) {
		_videoCapture.open(path);
		if(!_videoCapture.isOpened()){
			cout << "Error opening video stream or file" << endl;
			return;
		}
		_initialized = true;
	}

	~OpenCVLibrary() {
		_videoCapture.release();
		destroyAllWindows();
	}

	bool initialized() {
		return _initialized;
	}

	bool getNextFrame() {
		return _videoCapture.read(_current_frame);
	}

	void displayCurrentFrame() {
		cout << "frame height:" << _current_frame.size().height << " - width:" << _current_frame.size().width << endl;
		imshow( "Current Frame", _current_frame);
		_frame_height = _current_frame.size().height;
		_frame_width = _current_frame.size().width;
	}

	bool isExitCharPressed(){
		if(waitKey(0) == 27) {
			return true;
		}
		return false;
	}

	bool applyFilter(int x, int y, int height, int width){
		if((x + width) > _frame_width || (y + height) > _frame_height) {
			return false;
		}

//		if(x != 0 && (x - width) < 0) {
//			return false;
//		}
//
//		if(y != 0 && (y - height) < 0) {
//			return false;
//		}

//		cout << "_frame_height:" << _frame_height << " height:" << height << endl;
//		cout << "_frame_width:" << _frame_width << " width:" << width << endl;
//
//		if(_frame_height % height == 0 || _frame_width % width == 0) {
//			return true;
//		}
		return true;
	}

	bool decodeFrame(){
		// Display original image
		imshow("original Image", _current_frame);

		// Convert to graycsale
		Mat img_gray;
		cvtColor(_current_frame, img_gray, COLOR_BGR2HSV);
		imshow("img_gray", img_gray);

		Mat bgr[3];
		split(img_gray, bgr);

		imshow("bgr[0]", bgr[1]);

		Mat img_thres;
		threshold(bgr[1], img_thres, 50, 255, THRESH_BINARY_INV);
		imshow("img_thres", img_thres);

		vector<vector<Point> > contours;
        findContours(img_thres, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

		Mat drawing = Mat::zeros( img_gray.size(), CV_8UC3 );
		int biggest = 0;

		int BiggestContourArea = 0;

		vector<vector<Point> > contours_poly( contours.size() );
		vector<Rect> boundRect( contours.size() );

		for (int i = 0; i != contours.size(); i++){
			auto area = contourArea(contours[i]);
			if(area > 1000 && area < 10000) {
				biggest = area;
				BiggestContourArea = i;

			    double len1 = arcLength(contours[i], true);
				cout << "Len:" << len1 <<   "new biggest area :" << biggest << "index : " << BiggestContourArea << endl;

				vector<vector<Point> > approx;
			    approx.resize(contours.size());

				approxPolyDP(contours[i], approx[i], 0.02* len1, true);

				drawContours( drawing, contours, (int)i, (255,0,0), 1, LINE_AA);

				approxPolyDP( contours[i], contours_poly[i], 3, true );
				boundRect[i] = boundingRect( contours_poly[i] );
				if(applyFilter(boundRect[i].x, boundRect[i].y, boundRect[i].height, boundRect[i].width)) {
					cout << "XXX found match " << endl;
					rectangle( drawing, boundRect[i].tl(), boundRect[i].br(), (255,255,255), 2 );
					cout << "rect height:" << boundRect[i].height << " width:" << boundRect[i].width << endl;
				}
			}
		}
		imshow("Drawing 1", drawing);

		return 0;
	}
};

int main(int argc, char** argv )
{
	if(argc != 2) {
		cout << "usage: ./VideoMosaic <Video_Path> " << endl;
		return -1;
	}

	OpenCVLibrary openCvLib(argv[1]);
	if(!openCvLib.initialized()) {
		return -1;
	}

	while(openCvLib.getNextFrame() && !openCvLib.isExitCharPressed()){
		openCvLib.displayCurrentFrame();

		openCvLib.decodeFrame();
	}
	return 0;
}
